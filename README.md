# Eyes

A collection of AI projects for updating my skills. Mainly computer vision, hence the name. The goal is to catch-up with modern practices.

## Installation

I use `uv` for package management.

## Usage

TBD

## License

Not sure how the dependencies are licensed. This is copyleft.
