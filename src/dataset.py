import os
import tempfile
import zipfile

from huggingface_hub import snapshot_download
import json
import skimage
import torch

import lightning as L
import torch.utils.data as data
from lightning.pytorch.demos.boring_classes import RandomDataset


class AnnotatedImageDataset(data.Dataset):
    def __init__(self):
        dataset_fname = snapshot_download(repo_id="keremberke/garbage-object-detection", repo_type="dataset")
        train_data = os.path.join(dataset_fname, "data", "train.zip")
        self.zip_file = zipfile.ZipFile(train_data)
        self.zip_filelist = [x.filename for x in self.zip_file.filelist if x.filename.endswith(".jpg")]
        self.annotations_file = [x.filename for x in self.zip_file.filelist if x.filename.split(".")[-1] == "json"]
        assert len(self.annotations_file) == 1
        self.temp_dir = tempfile.TemporaryDirectory()
        self.temp_dir.__enter__()
        self.annotations_file = self.zip_file.extract(self.annotations_file[0], self.temp_dir.name)
        self.annotations = self._group_annotations_by_id(json.load(open(self.annotations_file)))

    def _group_annotations_by_id(self, annotations):
        by_id = {}
        for annot in annotations["annotations"]:
            image_id = annot["image_id"]
            if image_id not in by_id:
                by_id[image_id] = []
            by_id[image_id].append(annot)
        return by_id

    def __getitem__(self, ix):
        file_info = self.zip_filelist[ix]
        fname = self.zip_file.extract(file_info, self.temp_dir.name)
        image = skimage.io.imread(fname)
        annotations = self.annotations[ix]
        return image, annotations

    def __del__(self):
        self.zip_file.close()
        self.temp_dir.__exit__(None, None, None)


class TrainingDataModule(L.LightningDataModule):
    def prepare_data(self):
        self.dataset = AnnotatedImageDataset()

    def setup(self, stage):
        # make assignments here (val/train/test split)
        # called on every process in DDP
        dataset = RandomDataset(1, 100)
        self.train, self.val, self.test = data.random_split(
            dataset, [80, 10, 10], generator=torch.Generator().manual_seed(42)
        )

    def train_dataloader(self):
        return data.DataLoader(self.train)

    def val_dataloader(self):
        return data.DataLoader(self.val)

    def test_dataloader(self):
        return data.DataLoader(self.test)
