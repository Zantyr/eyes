import os

import lightning as L
from ultralytics import YOLO


class LightningYolo(L.LightningModule):
    def __init__(self):
        super().__init__()
        os.makedirs("models", exist_ok=True)
        self.core = YOLO(model="models/yolov8n.pt")

    def forward(self, image):
        return self.core.model.forward(image)
