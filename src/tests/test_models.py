import torch

from src.model import LightningYolo


def test_model_construction_and_forward():
    random_image = torch.rand([1, 3, 64, 64])
    model = LightningYolo()
    breakpoint()
    bbox_and_classification, (detection_1, detection_2, detection_3) = model.forward(random_image)
    # bbox_and_classification = [batch, (x, y, width, height) + 80classes, num_detections]
    # detections are some masks? but why 144? — there are some "mask weights"
