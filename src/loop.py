from .dataset import TrainingDataModule
from .model import LightningYolo

import lightning as L


def main():
    datamodule = TrainingDataModule()
    model = LightningYolo()
    trainer = L.Trainer()
    trainer.fit(model, datamodule=datamodule)
